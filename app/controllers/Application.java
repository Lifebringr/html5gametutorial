package controllers;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import play.mvc.Controller;

public class Application extends Controller {

    public static void index() {
        render();
    }
    public static void robotwalk() {
    	render();
    }
    
    public static void loadDataFile(String dataFile) throws IOException {
    	renderJSON(FileUtils.readFileToString(new File("data/" + dataFile)));
    }

}